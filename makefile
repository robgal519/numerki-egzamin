.PHONY: clean
NAME = Opracowanie2017

all: $(NAME).pdf

$(NAME).pdf: *.tex tex/*.tex
	pdflatex -shell-escape $(NAME).tex && pdflatex -shell-escape $(NAME).tex

run: $(NAME).pdf
	atril $(NAME).pdf

clean:
	@rm *.aux *.log *.out *.toc

